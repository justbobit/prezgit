DOCNAME=prezGit

all: prezGit

.PHONY: clean

prezGit:
	pdflatex -quiet -c-style-errors -halt-on-error -synctex=1 --shell-escape $(DOCNAME).tex
	pdflatex -quiet -c-style-errors -interaction=nonstopmode -synctex=1 --shell-escape $(DOCNAME).tex

view: prezGit
	open $(DOCNAME).pdf

clean:
	rm *.blg *.bbl *.aux *.log